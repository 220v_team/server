var express = require('express');
var app = express();
var http = ('http');
var path = ('path');
var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
require('./model/user');
require('./model/tags');
require('./model/rating');
require('./model/msg');
var Message = require('mongoose').model('message');
var Rating = require('mongoose').model('rating');
var Tag = require('mongoose').model('tags');
var User = require('mongoose').model('user');
var flash = require('connect-flash');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var mongoose = require('mongoose');

//end of dependencies

mongoose.connect('mongodb://yurka:orgish@ds055565.mlab.com:55565/job');
var db =mongoose.connection;


app.use(cookieParser('yurka'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


//connect to db
db.on('error', console.error.bind(console,'connection error'));
db.once('open',function() {
    console.log('Connected');
});

app.use(function(req,res,next) {
     res.header("Access-Control-Allow-Origin", "http://localhost:3000");
     res.header("Access-Control-Allow-Credentials", true);
     res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Cookie, Origin");

     if ('OPTIONS' == req.method) {
           res.sendStatus(200);
     } else {
          next();
     }
 });

app.use(function(req,res,next){
   if((!req.cookies) || (!req.cookies.cookie)) {
      next();
      return;
   }
   User.findOne({'_id': req.cookies.cookie},function(err,user){
       if(!err) {
         req.user = user;
       }
      next();
   });
});

app.post('/login', function(req,res) {
    var login = req.body.login;
    var password = req.body.password;
        return User.findOne({'login': login, 'password': password},{password: 0 }, function(err,data){
          if (err){ res.send('Извини, но ты делаешь что-то не так в этой жизни')}
          if (data == null){
                res.sendStatus(403);
          } else {
              res.cookie('cookie',data.id,{maxAge: 9999999, httpOnly: true, path: false});
              res.json(data);
          }
       });
});

app.get('/login', function(req,res){
       if (req.user == undefined) return res.sendStatus(403);
       res.json(req.user);
});

app.post('/signup',function(req,res){
   var login = req.body.login;
   var user = new User(req.body);
       return User.findOne({'login': login}, function(err, data){
            if(data && data.login == login){
                return res.send('User exist');
            } else {
                user.save(function(err){
                    if(!err){
                        return res.json(user)
                    }else{
                        res.send('Oops Error:' + err);
                    }
                });
            }
        });
});

app.get('/logout', function(req,res ){
    res.clearCookie('cookie');
    res.send('cookie has been successfuly deleted');
});

//profile users
app.get('/users/:id/', function(req,res){
   var id = req.params.id;
   if(id){
       User.findOne({'_id': id},{'password': 0}, function(err,data){
         Rating.find({'user': id},function(err,rate){
               var rating =0;
               for(var i=0; i<rate.length; i++) {
                   rating = rating + (rate[i].rating || 0);
               }
               if (rate.length>0) rating = rating / rate.length;
               if(data && data.rating){
                   data.rating = Math.round(rating)
                } else{ 
                       data.rating = 0
                    };
               var user = new User(data);
               if(!err){
                   user.save();
                   return res.json({user:data,rate:rate})
               };
               return res.send(err);
   })
//   if(!err) {return res.json(data)};
//   return res.send(err);
});
}

});

app.get('/rate', function(req,res){
    return User.find({}).where('rating').gt('5').limit(5).exec(function(err,data){
     if (!err) return res.json(data);
     return res.send(err)      
    })
});

app.get('/users/:id/message', function(req,res){
    
});

//settings
app.post('/users/:id', function(req,res){
    var id = req.body._id;
    var data = req.body;

        User.findOneAndUpdate({_id: id},data,function(err){
                if (!err) return res.sendStatus(200);
                return res.send(err)
        });

});

app.post('/:id/comments', function(req,res){

  var data = req.body;
  var rating = new Rating(data);
  rating.save(function(err){
      if (err) {return res.send('Все плохо')}
      return res.sendStatus(200);
  })
});

app.get('/search', function(req,res){
    var criteria = {};

    if (req.query.q) {
        criteria.name = {$regex: new RegExp(req.query.q)}
    };
    if (req.query.tag) {
        criteria.tag = {$regex: new RegExp(req.query.tag.split(' ').join('|'))}
    };

        return User.find(criteria,function(err,date){
            if (!err)  return res.json(date);
            return res.send(err);
    });

});

//response all users
app.get('/users', function(req, res) {
     User.find({},{"password": 0},function(err,date){
          if(err){return res.send(err)};
          return res.json(date);
     });
     
});

var port = process.env.PORT || 8080;

app.listen(port, function() {
  console.log("Listening on " + port);
});
