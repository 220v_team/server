var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var d = new Date;
var datenow = d.toUTCString();
var userSchema = new Schema({
        login: {
            type: String,
            unique: true
        },
        password: String,
        sex: {type: String, default: 'male'},
        location: {
            type:String,
            position: String,
            marker: Array
        },
        company: {type :Boolean,
                default: 'false'
        },
        picture: {
            type:String,
            default: 'http://cs402225.vk.me/v402225355/335f/0S3hgdCzbaA.jpg'
        },
        name: String,
        description: String,
        birthday: {
            type: Date,
            default: Date.now
        },
        dateRegistration: {
            type: Date,
            default: datenow
        },
        contacts: {
            phone: Number,
            mail: String,
            facebook: String,
            vk: String,
            webpage: String,
            twitter: String
        },
        tags: {
            type: String,
            default: String
        },
        rating: Number
});
mongoose.model('user', userSchema);


