var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tagsSchema = new Schema({
    name: String
});

mongoose.model('tags', tagsSchema);
