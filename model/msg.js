var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var msgSchema = new Schema({
    to: String,
    ts: String,
    msg:String
});

mongoose.model('message', msgSchema);
