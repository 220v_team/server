var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ratingSchema = new Schema({
    userRated: String,
    user: String,
    title: String,
    text: String,
    userName: String,
    userAvatar: String,
    rating: Number,
    date: String
});

mongoose.model('rating', ratingSchema);
